import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TasksController } from './tasks/tasks.controller';
import { TasksService } from './tasks/tasks.service';
import { CarsController } from './cars/cars.controller';
import { CompaniesController } from './companies/companies.controller';
import { ContactsController } from './contacts/contacts.controller';
import { CompaniesService } from './companies/companies.service';

@Module({
  imports: [],
  controllers: [AppController, TasksController, CarsController, CompaniesController, ContactsController],
  providers: [AppService, CompaniesService],
})
export class AppModule {}
