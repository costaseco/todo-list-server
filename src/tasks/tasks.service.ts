import { Injectable } from "@nestjs/common";

export interface Task { text: string, done: boolean }

let initialTasks = [{ text: "One", done: false}]

@Injectable()
export class TasksService {
    private readonly tasks: Task[] = initialTasks;

    findAll(): Task[] {
        console.log(this.tasks)
        return this.tasks;
    }

    create(task:Task) {
        this.tasks.push(task)
    }

    update(i:number) {
        this.tasks[i] = { ...this.tasks[i], done: !this.tasks[i].done} 
        console.log(this.tasks[i])
    }
}