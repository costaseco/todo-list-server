import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';

interface Task { text: string, done: boolean }

let tasks = [{ "text": "Download Lecture", "done": false }]

@Controller('tasks')
export class TasksController {

    @Get()
    // this ^^annotation means that the URL /tasks with method GET will be redirected to this function.
    // The result is a list of all tasks present in the server.
    getAllTasks(): Task[] {
        return tasks
    }

    @Post()
    // this ^^annotation means that the URL /tasks with method POST will be redirected to this function.
    // The result will 
    createNewTask(@Body() task: Task): void {
        //@Body indicates that nest should retrieve an object of type Task from the body of the request
        tasks.push(task)
    }

    @Get(':id')
    // this ^^annotation means that a request GET on URL /tasks/:id will be redirected to 
    // the method defined below. The result will be of type Task.
    getOneTask(@Param('id') id: number): Task {
        return tasks[id]
    }

    @Put(':id')
    // this ^^annotation means that a request PUT on URL /tasks/:id will be redirected to 
    // the method defined below. The result will be an effect in the state of the server 
    // with a toggled value in the corresponding task.
    toggleTask(@Param('id') id: number) {
        // In this case we are using the index in the array as a tasks identifier.
        tasks[id] = { ...tasks[id], done: !tasks[id].done }
        console.log(id)
    }
}
