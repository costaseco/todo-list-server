import { Controller, Get, HttpStatus, Param, Query, Res } from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { Response } from 'express'

// This controller serves URLs under the path "/companies". All methods defined inside
// will add components to the path. There is a dependency to a service that is implemented
// in class CompaniesService.

// This class was created by the command: nest generate controller companies

@Controller('companies')
export class CompaniesController {

    // This constructor defines not only a field of type CompaniesService 
    // but also a dependency to that service. Nest will create the link between the two.
    constructor(private companies:CompaniesService) {}

    @Get()
    // http://somename.io/companies
    // http://somename.io/companies?search=something
    getAllCompanies(@Query("search") search:string) {
        //   ^^ The annotation @Query in the parameter means that the value for search
        // is connected to the value used in the query string of the URL 
        return this.companies.getAllCompanies(search)
        // ^^ By directly returning a value the controller will issue a 200 OK code and 
        // place this value in the body of the response.
    }

    @Get(":id")
    // http://somename.io/companies/EDP
    getOneCompany(@Param("id") id: string, @Res() res: Response) {
        //   ^^ The annotation @Param in the parameter means that the value for search
        // is connected to the value used in the path indicated by ":id" in the URL.
        //   ^^ The annotation @Res means that the object res representing the full
        // response to the current request is available and can be used to build a 
        // custom response.
        const company = this.companies.getOneCompany(id)
        if (company === undefined) {
            res.status(HttpStatus.NOT_FOUND).send()
            // ^^ this statement ends the building of this response with a 404 NOT Found status code.
        }
        else 
            res.status(HttpStatus.OK).json(company)
            // ^^ This statement represents the regular exit point of a controller, with the 200 OK status code
    }
}
