import { Injectable } from '@nestjs/common';

// This service defined here concentrates the linked information between companies and contacts
// We define here the datatypes for Contacts and Companies. Notice that the contacts defined 
// are owned by companies, ie. there is a list of Contacts stored inside each object of type Company.

// This class was created by the command: nest generate service companies

export interface Contact { name: string, email:string }
export interface Company { name: string, contacts:Contact[]}

// This collection of companies is to be replaced by a connection to the database
const initialCompanies = 
    [
        {name:"EDP", 
         contacts:
            [
                {name:"one at edp", email:"one@edp"}
            ,   { name:"two at edp", email:"two@edp"}
            ,   { name:"three at edp", email:"three@edp"}
            ]
        }
    ,   {name:"RTP", 
         contacts:
            [
                {name:"one at rtp", email:"one@rtp"}
            ,   {name:"two at rtp", email:"two@rtp"}
            ,   {name:"three at rtp", email:"three@rtp"}
            ]
        }
    ]

@Injectable()
// This service (also known as Provider) implements the logic that actually defines
// the behaviour of this application. It also defines the collection of objects that
// temporarily replaces here the data layer of the application. 
export class CompaniesService {

    // This collection should be replaced by a connection to a database
    private readonly companies:Company[] = initialCompanies

    // This method returns the filtered list of companies, given a search 
    // criteria (in this case a sub-string)
    getAllCompanies(search:string):Company[] {
        if( search === undefined )
            return this.companies
        else 
            return this.companies.filter( (c) => c.name.includes(search) )
            // ^^ This expression uses a lambda (a function defined inline) to filter the list.
            // All companies (c) that yield true for the expression `c.name.includes(search)`
            // will be included in the result.
    }

    // This method is similar but for a summarised list of contacts
    getAllContacts(search: string):Contact[] {
        const fulllist = this.companies.reduce((contacts, company) => [...contacts,...company.contacts], [] )
        // ^^ This expression collects all the contacts present in all companies. Function reduce
        // accepts a function that takes an acumulated value and an element and returns a new acumulated value
        // after processing the present element. The final result is the list of all contacts.
        if (search === undefined)
            return fulllist
        else
            return fulllist.filter((c) => (c.name.includes(search) || c.email.includes(search)))
            // ^^ The filter in this case also covers the email 
    }

    // Similar method to retrieve the elements of a single company
    getCompanyContacts(n:string):Contact[] {
        return this.companies.find( (c) => c.name === n ).contacts
    }

    // And to retrieve a company as a whole.
    getOneCompany(n:string):Company {
        return this.companies.find((c) => c.name === n)
    }
}
