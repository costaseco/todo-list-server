import { Controller, Get, Query } from '@nestjs/common';
import { CompaniesService, Contact } from '../companies/companies.service';

@Controller('contacts')
export class ContactsController {

    constructor(private companies: CompaniesService) { }

    @Get()
    // http://somename.io/contacts
    // http://somename.io/contacts?search=something
    getAllContacts(@Query("search") search:string):Contact[] {
        return this.companies.getAllContacts(search)
    }

}
